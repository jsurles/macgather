## Cobbled together by jsurles to help troubleshoot/diagnose remote mac user system and connectivity issues.
TSTAMP=`/bin/date +%Y%m%d_%H%M%S`
FILEOUT="/tmp/macgather_out-$TSTAMP.txt"


clear

NOTIFY () {
  /bin/echo "################################################################################################################"
  /bin/echo "Data has been collected about your system and sent to your admins."
  /bin/echo "################################################################################################################"
}

NL () {
  echo >> $FILEOUT
}

TITLE () {
  echo "===> $1 <===" >> $FILEOUT
  echo "RUNNING: $1 "
}

RUN () {
  TITLE "$1"
  sh -c "$1" >> $FILEOUT 2>&1
  NL
}

STATIT () {
  TITLE "/usr/bin/stat $1"
  /usr/bin/stat "$1" >> $FILEOUT 2>&1
  NL
}

DUMPIT () {
  TITLE "/bin/cat $1"
  /bin/cat "$1"  >> $FILEOUT 2>&1
  NL
}

FILEDATA () {
  if [ -e "$1" ];then
    STATIT "$1"
    DUMPIT "$1"
  else
    echo "$1 does not exist." >> $FILEOUT
    NL
  fi
}

## Gather System Information

RUN "/bin/date"
RUN "/usr/bin/uptime"
RUN "/bin/hostname"
RUN "/usr/bin/uname -a"
RUN "/bin/df -h"
RUN "/bin/df -i"
RUN "/sbin/ifconfig -a"
RUN "/usr/sbin/netstat -r"
RUN "/usr/sbin/netstat -rn"
RUN "/usr/sbin/netstat -an"
FILEDATA "/etc/resolv.conf"
FILEDATA "/etc/hosts"
RUN "/usr/sbin/scutil --dns"
RUN "/usr/bin/top -l3 -n 50 | tail -62"
RUN "/bin/ps auxwww"
RUN "/usr/sbin/lsof"

## Gather Connectivity Information

RUN "/usr/bin/curl -s -4 http://ifconfig.co"
RUN "/sbin/ping -c5 www.google.com"
RUN "/sbin/ping -c5 itrip.itpops.com"
RUN "/usr/bin/nc -vz itrip.itpops.com 80"
RUN "/usr/bin/nc -vz itrip.itpops.com 443"
RUN "/usr/bin/dig +trace www.google.com"
RUN "/usr/bin/dig +trace itrip.itpops.com"
RUN "/usr/bin/curl -s http://itrip.itpops.com"
RUN "/usr/bin/curl -k -s https://itrip.itpops.com/login.xhtml"
RUN "/usr/bin/curl -k -s https://itrip.itpops.com/login.xhtml --resolve itrip.itpops.com:443:66.150.136.42"
RUN "/usr/bin/curl -k -s https://itrip.itpops.com/mac-gather.txt"

NOTIFY
